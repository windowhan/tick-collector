(function(){
	setTimeout(function(){
		var global_websocket = new WebSocket("wss://45.76.215.119:8003/web-socket/");
		global_websocket.onopen = function(event)
		{
			console.log("onopen event created...");
			//global_websocket.send("connected..");
		};

		var symbol = document.title.split(":")[0];
		console.log("start");
		var price_span = document.getElementsByClassName("dl-header-price")[0];
		price_span.addEventListener('DOMSubtreeModified', function(){
			var obj = new Object();
			obj.symbol = symbol;
			obj.price = parseFloat(price_span.innerText);
			var output = JSON.stringify(obj);
			if(output.indexOf("null")>-1)
				return;
			console.log(output);

			global_websocket.send(output);
		});

	}, 100);
	
})();
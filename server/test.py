# Note this is targeted at python 3
import ssl
import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.websocket
import tornado.options
import datetime, time, json
from timeseries2redis import TickData

LISTEN_PORT = 8003
LISTEN_ADDRESS = '0.0.0.0'


class ChannelHandler(tornado.websocket.WebSocketHandler):
    """
    Handler that handles a websocket channel
    """
    @classmethod
    def urls(cls):
        #print(cls)
        return [
            (r'/web-socket/', cls, {}),  # Route/Handler/kwargs
        ]
    
    def initialize(self):
        self.channel = None
        

    def open(self):
        """
        Client opens a websocket
        """
        print("[" + str(datetime.datetime.now()) + "] Client Connected")
        #print dir(self)
    
    def on_message(self, message):
        """
        Message received on channel
        """
        print("[" + str(datetime.datetime.now()) + "] " + message)
        raw_data = json.loads(message)
        ts = TickData(raw_data["symbol"], structure=["date", "price"])
        date = datetime.datetime.fromtimestamp(int(time.time()))
        price = raw_data["price"]

        data = {"date":str(date), "price" : price}
        ts.set(data)
        #print("[" + str(datetime.datetime.now()) + "] " + message)
    
    def on_close(self):
        """
        Channel is closed
        """
        print("[" + str(datetime.datetime.now()) + "] Client Disconnected")
    
    def check_origin(self, origin):
        """
        Override the origin check if needed
        """
        return True
    
    
def main():
    # Create tornado application and supply URL routes
    app = tornado.web.Application(ChannelHandler.urls())
    ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_ctx.load_cert_chain("./cert.pem", "./key.pem")
    # Setup HTTP Server
    http_server = tornado.httpserver.HTTPServer(app, ssl_options=ssl_ctx)
    http_server.listen(LISTEN_PORT, LISTEN_ADDRESS)
    print("[" + str(datetime.datetime.now()) + "] Listen Address : " + LISTEN_ADDRESS)
    print("[" + str(datetime.datetime.now()) + "] Listen Port : " + str(LISTEN_PORT))
    # Start IO/Event loop
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    main()
